"""some utilties for working with iterables"""

def chunks(iterable, size):
  """Yield successive n-sized chunks from an iterable."""
  for i in range(0, len(iterable), size):
    yield iterable[i:i + size]


def cleave(string, char, anterior=True):
  """cleave off a portion of a string at the first
     occurence of the specified value.

     USAGE:
         cleave(name@gmail.com, '@')
         >>> name

         cleave(name@gmail.com, '@', anterior=False)
         >>> gmail.com

      ARGUMENTS:
          string: str: target of the operation
          char: str: character at which the slice occurs
          anterior: bool: determines which portion of the string to return
  """
  if char in string:
    pos = string.find(char)
    result = string[:pos] if anterior is True else string[pos + 1:]
    return result
  return string
