import json
import requests
from sys import stdout
from random import choice
from shutil import get_terminal_size
from requests.exceptions import RequestException

from src.utils.system import HOME
from src.utils.system import withprogress


@withprogress
def download(url, filename=None, dir=None):
    """download a file from the network."""
    filename = url.split('/')[-1] if filename is None else filename

    try:
        connection = requests.get(url, stream=True)
        step = round(float(connection.headers['Content-Length']) / 100)

        def stream():
            with open(f"{dir}/{filename}", 'wb') as file:
                for index, chunk in enumerate(connection.iter_content(chunk_size=step)):
                    file.write(chunk)
                    yield index
        return stream()

    except RequestException as error:
        print(error)


def random_user_agent(*devices):
    """generate a random http request header.

        ARGUMENT:
            user_devices: str: 'mobile' or 'desktop'

        USAGE:
            random_user_agent('mobile')
            random_user_agent('desktop')
    """
    with open(f'{HOME}/src/confs/uagents.conf', 'r') as file:
        UA = json.load(file)
        browsers = UA[choice(devices)]
        header = {'User-Agent': browsers[choice(list(browsers))]}
        return header
