"""network connections and html parsing operations"""
import re
import pandas as pd
from os import system
from math import trunc
from requests_html import HTMLSession

from src.utils import iterops
from src.utils.constants import USER_AGENT, URLS
from src.utils.constants import SYNOPSIS_WRAPPER
from src.utils.constants import SESSION, OMISSIONS


def get_header(html):
    """Returns the results page header text"""
    tables = html.find('table')
    return iterops.cleave(tables[1].text, '\n')


def gather_links(html, header):
    """Collects the links to every page of search results"""
    links = []
    nextpage = [link for link in html.absolute_links if link.endswith('page=2')]
    if nextpage:
        link_template = nextpage[0][:-1]
        num_of_results = re.match(r'\d{1,6}', header).group(0)
        num_of_pages = trunc(int(num_of_results) / 50)
        page_numbers = list(range(1, num_of_pages + 2))
        for number in page_numbers:
            links.append(f'{link_template}{number}')
    return links


def extract_md5(link):
    """Extract the book title hash from the book link"""
    pos = link.find('=') + 1
    return link[pos:]


def gather_books(html):
    """Gather all titles and links from the current page"""
    def gather_mirrors(elements, md5):
        mirrors = {}
        for element in elements:
            if md5 in element.attrs['href']:
                if not element.attrs['title']:
                    continue
                else:
                    mirrors.update({element.attrs['title']: element.attrs['href']})
        return mirrors

    table = html.find('.c')
    links = table[0].find('a')

    book_results = {}
    book_links = [link for link in links if 'book/index.php?md5=' in link.attrs['href']]

    for number, element in enumerate(book_links, 1):
        md5_hash = extract_md5(element.attrs['href'])
        result = {
            str(number): {
                'md5': md5_hash,
                'title': iterops.cleave(element.text, '\n'),
                'link': element.absolute_links.pop(),
                'mirrors': gather_mirrors(links, md5_hash)
            }}
        book_results.update(result)
    return book_results


def gather_data(html):
    table = html.find('.c')
    td = table[0].find('td')

    headers = [tag.text for tag in td[:9]]
    body = [tag.text for tag in td[11:] if tag.text not in OMISSIONS]

    chunked = iterops.chunks(body, 9)
    results = {item: [] for item in headers}
    for chunk in chunked:
        for number, item in enumerate(chunk):
            if number == 2:
                results[headers[number]].append(iterops.cleave(item, '\n'))
            else:
                results[headers[number]].append(item)

    return pd.DataFrame(results, index=range(1, len(results['Title']) + 1))


def harvest_data(html):
    head = get_header(html)
    books = gather_books(html)
    dataframe = gather_data(html)
    return {'header': head, 'df': dataframe, 'books': books}


def search(query):
    parameters = {'req': query.replace(' ', '+'), 'res': '50'}
    response = SESSION.get(URLS['3'], params=parameters, headers=USER_AGENT)
    response.raise_for_status()
    if response.ok:
        page1 = harvest_data(response.html)
        pages = gather_links(response.html, page1['header'])
        cache = {'1': page1}

    return cache, pages


def fetch_page(url, cache):
    pnum = url[-1] if re.search(r'\d{1,6}$', url) else '1'
    if pnum in cache:
        result = cache[pnum]
    else:
        page = SESSION.get(url, headers=USER_AGENT)
        result = harvest_data(page.html)
        cache.update({pnum: result})
    return result


def display_results(dataframe, pageheader, pagenumber, view):
    system('clear')
    print(f'PAGE {pagenumber}:')
    print(f'\n{pageheader.upper()}\n')
    # display(dataframe[['Author(s)', 'Title', 'Extension']])
    with pd.option_context('display.max_colwidth', 100):
        print(dataframe[view])


def select_title(selection, cache):

    md5 = cache['books'][selection]['md5']
    title = cache['books'][selection]['title']
    url = cache['books'][selection]['mirrors'].get('Libgen.gs', 'this mirror')

    response = SESSION.get(url)

    html = response.html
    textobjs = [td for td in html.find('td') if 'colspan' in td.attrs]
    dload = [link for link in html.absolute_links if md5.lower() in link]

    system('clear')
    print(f'{title}\n')

    synopsis = textobjs[1].text
    if synopsis:
        print(SYNOPSIS_WRAPPER.fill(text=synopsis))
    else:
        print('No Synopsis')

    return dload[0], title.replace(' ', '-')
