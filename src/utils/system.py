"""system interface and file operations"""
import re
import os
import sys
import shutil
import platform
import contextlib
from sys import stdout
import subprocess as sp

HOME = sys.path[0]


def devnull(func):
    """redirect stderr to /dev/null"""
    def nullified(*args, **kwargs):
        with open(os.devnull, 'w') as null:
            with contextlib.redirect_stderr(null):
                func(*args, **kwargs)
    return nullified


def add_extension(document):
    """apply correct file extension using file introspection."""
    system = platform.system()
    cmd = 'file -I' if system.lower() == 'darwin' else 'file -i'
    status = sp.run(f'{cmd} {document}', shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    status = status.stdout.decode()
    try:
        extension = re.search(r'(/epub|/pdf|/octet|octet-stream|\.djvu)', status, re.IGNORECASE).group(0).strip('/')
        extension = 'mobi' if extension == 'octet' else extension
        extension = 'azw3' if extension == 'octet-stream' else extension
        shutil.move(document, f'{document}.{extension.lower()}')

    except AttributeError:
        stdout.write('download successful, but unable to add the file extension\n')


def progressbar(inc, bar='=', space=' '):
    cols, _ = shutil.get_terminal_size()
    progress = bar * inc
    timeleft = space * (cols // 2)
    stdout.write(f"[{progress}{timeleft[:-inc]}]\r")
    stdout.flush()


def withprogress(generator):
    """progress bar decorator"""
    def progress(*args, **kwargs):
        routine = generator(*args, **kwargs)
        while True:
            try:
                step = next(routine)
            except StopIteration as exception:
                stdout.write('\n')
                return exception.value
            else:
                progressbar(step)
    return progress
