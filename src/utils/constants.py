import json
import textwrap
from requests_html import HTMLSession
from src.utils.webtools import random_user_agent

SESSION = HTMLSession()
USER_AGENT = random_user_agent('desktop')
SYNOPSIS_WRAPPER = textwrap.TextWrapper(width=105)
OMISSIONS = ('[1]', '[2]', '[3]', '[4]', '[5]', '[edit]')

APROMPT = "\nType 's' to select a title or 'q' to exit: "
BPROMPT = "\nType a number bewteen 1 and {0} to view another page; \
's' to select a title or 'q' to exit: "

CPROMPT = "\nType the number of the title that you are interested in: "
DPROMPT = "\nWould you like to download this book? (y, n): "
EPROMPT = "\nWould you like to search for another title in this session? (y, n): "
FPROMPT = "\nType your Query: "

URLS = {
    "1": "http://93.174.95.27/search.php?",
    "2": "http://gen.lib.rus.ec/search.php?",
    "3": "https://libgen.is/search.php?"
}

VIEWOPTIONS = {
    'id': 'ID',
    'author': 'Authors(s)',
    'title': 'Title',
    'pub': 'Publisher',
    'year': 'Year',
    'pages': 'Pages',
    'lang': 'Language',
    'size': 'Size',
    'ext': 'Extension'
}
