"""main application user interface logic"""
import sys
from os import system
from requests import exceptions

from src.utils.constants import SESSION
from src.utils.constants import APROMPT, BPROMPT, CPROMPT
from src.utils.constants import DPROMPT, EPROMPT, FPROMPT

from src.utils.scraper import search, display_results
from src.utils.scraper import fetch_page, select_title

from src.utils.webtools import download
from src.utils.system import add_extension


def api(book, location, view):
    """the application interface"""
    try:
        page_number = '1'
        webcache, all_pages = search(book)

    except exceptions.RequestException as error:
        print(f'An error has occured.\n{error}')

    else:
        webpage = webcache[page_number]
        display_results(webpage['df'], webpage['header'], page_number, view)

        numpages = len(all_pages)
        prompt = APROMPT if numpages <= 1 else BPROMPT.format(numpages)

        while True:
            choice = input(prompt).strip()
            if choice in ('q', 'quit', 'exit'):
                system('clear')
                break

            elif choice.isdigit() and int(choice) in range(1, numpages + 1):
                page_number = choice
                pnumber = int(choice) - 1
                webpage = fetch_page(all_pages[pnumber], webcache)
                display_results(webpage['df'], webpage['header'], choice, view)

            elif choice in ('s', 'select'):
                choice = input(CPROMPT)
                dnlink, doc = select_title(choice, webcache[page_number])

                while True:
                    choice = input(DPROMPT).strip()
                    if choice not in ('y', 'yes', 'n', 'no'):
                        print('Invalid option. Please type y or n: ')

                    elif choice in ('n', 'no'):
                        display_results(webpage['df'], webpage['header'], page_number, view)
                        break

                    elif choice in ('y', 'yes'):
                        _ = download(dnlink, filename=doc, dir=location)
                        add_extension(f"{location}/{doc}")
                        display_results(webpage['df'], webpage['header'], page_number, view)
                        break
            else:
                print('Invalid Option')
                continue


def app(query, path=None, display=None):
    """the main loop"""
    while True:
        api(query, path, display)
        system('clear')
        choice = input(EPROMPT).strip()
        if choice not in ('y', 'yes', 'n', 'no'):
            print('Invalid option. Please type y or n: ')
        elif choice in ('n', 'no', 'q', 'quit', 'exit'):
            break
        elif choice in ('y', 'yes'):
            query = input(FPROMPT).strip()

    SESSION.close()
    system('clear')
    sys.exit()
