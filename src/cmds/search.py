"""libgenesis-cli 1.1.0 [2020-03-21]

COMMAND:
    search

USAGE:
    VIEW DOCUMENTATION:

    libgen search help

    QUERY THE API FOR A BOOK, AUTHOR OR SUBJECT:

    libgen search query $title -d $directory -v title -v author -x ext

ARGUMENTS:
    query:   the search term (mandatory)
    -d       sets the download directory (defaults to ~/Downloads)
    -v       selects the attributes to display (optional)

VIEW ATTRIBUTES:

    id
    size
    title
    author
    lang  (language)
    pub   (publisher)
    year  (year published)
    pages (number of pages)
    ext   (file type (extension))
"""
import os
import sys
from src.utils import setopts
from src.utils.application import app
from src.utils.constants import SESSION, VIEWOPTIONS

this = sys.modules[__name__]


class search(setopts.SetOpts):
    """Library Genesis – also known as libgen – is a fantastic digital 
shadow library that gives you free access to millions of your favourite 
books and papers as eBooks – from fiction books to fantasy, crime to science 
fiction and romance to thriller, as well as textbooks, journal articles, 
academic works, graphic novels, comics etc – in epub, pdf, mobi and many 
other formats. Perfect for reading on your Kindle, ipad, Android or any 
e-reader device capable of reading books in any eBook format.

The servers are known to be slow to repsond, sometimes query can take
up to 30 seconds. It's not this application, as the same responsivness 
is experienced when using a browser. There are many mirrors to this site
and some are slower than others. Use the mirror command to choose which
url this app will query.

USAGE:
    VIEW DOCUMENTATION:

    library search help

    QUERY THE API FOR A BOOK, AUTHOR OR SUBJECT:

    library search query $title -d $directory -v title -v author -x ext

    EXAMPLES:
    for results displayed with the title, publisher and format:
        library search query Javascript -v title -v pub -v ext

    to download to a specific directory:
        library search query Javascript -d /full/path/to/directory

ARGUMENTS:
    query:   the search term (mandatory)
    -d       sets the download directory (defaults to ~/Downloads)
    -v       selects the attributes to display (optional)

VIEW ATTRIBUTES:

    id
    size
    title
    author
    lang  (language)
    pub   (publisher)
    year  (year published)
    pages (number of pages)
    ext   (file type (extension))

for more information visit library genesis @http://libgen.is/
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        views = VIEWOPTIONS.items()
        directory = str(self.options.d or os.path.expanduser('~/Downloads'))

        if self.options.v:
            view = [v for k, v in views if k in self.options.v]

        else:
            view = ['Title', 'Size', 'Extension']

        try:
            print('contacting the library api...')
            app(self.options.query, path=directory, display=view)

        except KeyboardInterrupt:
            SESSION.close()
            os.system('clear')
            sys.exit('session interrupted\nhttps socket closed')
