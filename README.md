## libgenesis-cli 

search and download data from Library Genesis via the command line

## Overview

Library Genesis – also known as libgen – is a fantastic digital 
shadow library that gives you free access to millions of your favourite 
books and papers as eBooks – from fiction books to fantasy, crime to science 
fiction and romance to thriller, as well as textbooks, journal articles, 
academic works, graphic novels, comics etc – in epub, pdf, mobi and many 
other formats. Perfect for reading on your Kindle, ipad, Android or any 
e-reader device capable of reading books in any eBook format.

NOTE: The servers are cknown to be slow to respond, sometimes a query can take
up to 30 seconds or more. It's not this interface, as the same responsivness 
is experienced when using a browser depending on the mirror used. There are 
many mirrors to this site and some are slower than others. 

Use the mirror command (once implemented) to choose which url this app will query. 

## Requirements

+ [Python](https://python.org/)
+ [pipenv](https://pipenv.pypa.io/en/latest/)

#### OR

+ [docker](https:docker.com/)

## Getting Started

### Use Docker
    
    docker pull thepromethean/libgenesis:slim
    docker run --name libgen -it --rm libgenesis
    
    $-> libgen help

### Manual install

    git clone https://gitlab.com/kafkaa/libgenesis-cli.git 
    cd libgenesis-cli
    pipenv install
    echo -e "#!$(pwd)/.venv/bin/python3\n$(cat main.py)" > main.py
    chmod u+x main.py 
    cd /usr/local/bin
    ln -s /full/path/to/main.py libgen

    $-> libgen help

## Usage

    DOCUMENTATION:

    libgen search help

    QUERY THE LIBRARY:

    libgen search query $title -d $directory -v title author size ext

    ARGUMENTS:

    query:   the search term (mandatory)
    -d       sets the download directory (defaults to ~/Downloads)
    -v       selects a view attribute to display as results (optional)

    VIEW ATTRIBUTES:

    id
    size
    title
    author
    lang  (language)
    pub   (publisher)
    year  (year published)
    pages (number of pages)
    ext   (file type (extension))


## Issues

+ the author view attribute is no longer working. fix pending.

## Reference

+ [Library Genesis](http://libgen.is/)

## License

+ [Apache](http://opensource.org/licenses/Apache-2.0)
