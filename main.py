"""libgenesis-cli 1.1.0 [2020-03-21]

USAGE:
    libgen search help
    libgen search [query] [-d, --directory] [-v, --views]

OPTIONS:
    -d,  sets the download directory (defaults to ~/Downloads)
    -v,  selects the view attributes (-v title -v author -v year -v ext)

VIEW ATTRIBUTES:

    id
    size
    title
    author
    lang  (language)
    pub   (publisher)
    year  (year published)
    pages (number of pages)
    ext   (file type (extension))

for more information visit library genesis @http://libgen.is/"""
import sys
import importlib

this = sys.modules[__name__]

if __name__ == '__main__':

    try:
        command = sys.argv[1]
        module = importlib.import_module(f'src.cmds.{command}')
        command = getattr(module, command)
        command = command(sys.argv[1:])
        command.execute()

    except (IndexError, ModuleNotFoundError):
        print(this.__doc__)
